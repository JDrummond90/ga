package com.optimisation;

import org.junit.Test;

import java.util.HashSet;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by jamie on 22/01/17.
 */
public class OptimiserTest {

    /**
     * Given diameter D
     * A = (π/4) × D^2
     */
    @Test
    public void Optimise_Circle_Unit_Area(){

        double targetArea = 4.253;

        Parameter diameterParameter = new Parameter("diameter", 0.1, 0, 20);
        FitnessFunction diameterArea = (x) ->
                Math.abs(targetArea - ((Math.PI / 4) * Math.pow(x.getParameter("diameter"), 2)));

        HashSet<Parameter> parameters = new HashSet<>();
        parameters.add(diameterParameter);

        Optimiser optimiser = new Optimiser(diameterArea, new GaussianMutator(), parameters);
        Map.Entry<Chromosome, Double> best = optimiser.optimise();

        System.out.println("Generated the best Chromosome: " + best + " in " + optimiser.getGenerations().size() + " generations");

        assertEquals(2.327, best.getKey().getParameter("diameter"), 0.01);
        assertEquals(0.001, best.getValue(), 0.01);
    }

}