package com.optimisation;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by jamie on 22/01/17.
 */
public class Optimiser {
    private FitnessFunction fitnessFunction;
    private Mutator mutator;
    private Set<Parameter> parameters;
    private Set<Generation> generations = new HashSet<>();
    private int generationSize = 20;
    private Double priorBest;
    private Map.Entry<Chromosome, Double> globalBest;

    private Random random = new Random();

    public Optimiser(FitnessFunction fitnessFunction, Mutator mutator, Set<Parameter> parameters) {
        this.fitnessFunction = fitnessFunction;
        this.mutator = mutator;
        this.parameters = parameters;
    }

    public Set<Generation> getGenerations() {
        return generations;
    }

    public Map.Entry<Chromosome, Double> optimise() {
        List<Chromosome> chromosomes = generateInitialChromosomes();

        Generation generation = new Generation(chromosomes);
        Map<Chromosome, Double> fitnesses;

        Map.Entry<Chromosome, Double> best;
        do {
            generations.add(generation);
            fitnesses = evaluateGeneration(generation);

            List<Map.Entry<Chromosome, Double>> sorted = fitnesses.entrySet().stream()
                    .sorted(Comparator.comparing(Map.Entry::getValue)).collect(Collectors.toList());
            best = sorted.get(0);
            generation = new Generation(evolve(best));
            System.out.println("Generation " + generations.size() + " best " + best);
        } while (!hasConverged(best));

        return globalBest;
    }

    private Map<Chromosome, Double> evaluateGeneration(Generation generation) {
        Map<Chromosome, Double> fitnesses = new HashMap<>();

        for (Chromosome chromosome : generation.getChromosomes()) {
            double fitness = fitnessFunction.fitness(chromosome);
            fitnesses.put(chromosome, fitness);
        }

        return fitnesses;
    }

    private List<Chromosome> generateInitialChromosomes() {
        List<Chromosome> chromosomes = new ArrayList<>();

        for (int i = 0; i < generationSize; i++) {
            Map<Parameter, Double> values = new HashMap<>();

            for (Parameter parameter : parameters) {
                double range = parameter.getMax() - parameter.getMin();
                double value = random.nextDouble() * range;
                values.put(parameter, value);
            }

            Chromosome chromosome = new Chromosome(values);
            chromosomes.add(chromosome);
        }

        return chromosomes;
    }

    private List<Chromosome> evolve(Map.Entry<Chromosome, Double> best) {
        return mutator.mutate(parameters, best.getKey(), generationSize);
    }

    private boolean hasConverged(Map.Entry<Chromosome, Double> best) {
        boolean converged = false;

        if (priorBest != null) {
            converged = Math.abs(best.getValue() - priorBest) < 0.001;

            if(converged){
                System.out.println("Converged, prior=" + priorBest + " new=" + best.getValue());
            }
        }
        if(globalBest == null || best.getValue() < globalBest.getValue()){
            globalBest = best;
        }

        priorBest = best.getValue();
        boolean found = best.getValue() <= 0.001;
        return converged || found;
    }
}
