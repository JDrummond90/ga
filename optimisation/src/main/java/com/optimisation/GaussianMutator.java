package com.optimisation;

import java.util.*;

/**
 * Created by jamie on 22/01/17.
 */
public class GaussianMutator implements Mutator {

    private Random random = new Random();

    @Override
    public List<Chromosome> mutate(Set<Parameter> parameters, Chromosome best, int number) {
        List<Chromosome> chromosomes = new ArrayList<>();

        for (int i = 0; i < number; i++) {
            Map<Parameter, Double> values = new HashMap<>();

            for (Parameter parameter : parameters) {
                Double bestValue = best.getParameter(parameter);
                double mutated = mutateValue(bestValue, parameter);
                values.put(parameter, mutated);
            }
            Chromosome chromosome = new Chromosome(values);
            chromosomes.add(chromosome);
        }
        return chromosomes;
    }

    private double mutateValue(Double bestValue, Parameter parameter) {
        return bestValue + parameter.getScale() * (random.nextBoolean() ? random.nextDouble() : random.nextDouble() * -1);
    }
}
