package com.optimisation;

import java.util.Map;

public class Chromosome {
    private final Map<Parameter, Double> parameterValues;

    public Chromosome(Map<Parameter, Double> parameterValues) {
        this.parameterValues = parameterValues;
    }

    public double getParameter(String name) {
        return parameterValues.entrySet().stream()
                .filter(x -> x.getKey().getName().equals(name))
                .map(x -> x.getValue()).findFirst().get();
    }

    public Double getParameter(Parameter parameter) {
        return parameterValues.get(parameter);
    }

    @Override
    public String toString() {
        return "Chromosome{" +
                "parameterValues=" + parameterValues +
                '}';
    }
}