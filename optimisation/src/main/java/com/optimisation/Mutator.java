package com.optimisation;

import java.util.List;
import java.util.Set;

/**
 * Created by jamie on 22/01/17.
 */
public interface Mutator {

    List<Chromosome> mutate(Set<Parameter> parameters, Chromosome best, int number);
}
