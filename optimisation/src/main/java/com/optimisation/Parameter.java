package com.optimisation;

/**
 * Created by jamie on 22/01/17.
 */
public class Parameter {

    private final String name;
    private final double scale;
    private final double min;
    private final double max;

    public Parameter(String name, double scale, double min, double max) {
        this.name = name;
        this.scale = scale;
        this.min = min;
        this.max = max;
    }

    public String getName() {
        return name;
    }

    public double getScale() {
        return scale;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    @Override
    public String toString() {
        return "Parameter{" +
                "name='" + name + '\'' +
                '}';
    }
}
