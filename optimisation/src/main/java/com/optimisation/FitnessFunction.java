package com.optimisation;

public interface FitnessFunction {

    double fitness(Chromosome chromosome);
}
