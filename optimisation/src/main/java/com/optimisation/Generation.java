package com.optimisation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jamie on 22/01/17.
 */
public class Generation {

    private List<Chromosome> chromosomes = new ArrayList<>();

    public Generation(List<Chromosome> chromosomes) {
        this.chromosomes = chromosomes;
    }

    public List<Chromosome> getChromosomes() {
        return chromosomes;
    }
}
